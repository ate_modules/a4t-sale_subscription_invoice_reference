# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class Subscription(metaclass=PoolMeta):
    "Subscription"
    __name__ = 'sale.subscription'

    def _get_invoice(self):
        invoice = super()._get_invoice()
        if invoice:
            invoice.reference = self.reference
        return invoice
