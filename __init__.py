# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import subscription


def register():
    Pool.register(
        subscription.Subscription,
        module='sale_subscription_invoice_reference', type_='model')
